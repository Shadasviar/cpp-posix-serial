BUILD_DIR=$(shell pwd)/build

all: build-test

clean:
	rm -rf ${BUILD_DIR}

test:
	${BUILD_DIR}/tests/tests

build:
	cmake -B${BUILD_DIR} .
	cmake --build ${BUILD_DIR}

build-test:
	cmake -B${BUILD_DIR}/tests  -DXUNIL_SERIAL_LIB_TYPE=STATIC tests
	cmake --build ${BUILD_DIR}/tests

cppcheck:
	cppcheck \
	--enable=all \
	src/* \
	-Iinclude
