## xunil-serial ##
This is a simple C++ library for serial port for linux systems.
This library is not portable.

### Killer features ###
- Support of custom baudrates
- Support of the iostream interface

### How to use ###

#### Building ####
This library is configured using CMake. By default, it is built as dynamic
library, but you can change it by setting `XUNIL_SERIAL_LIB_TYPE` cmake
variable to `STATIC`. You can add this library to your project
using `ADD_SUBDIRECTORY` function and linking as 'xunil-serial'. You can also
install it and add to project by `FIND_PACKAGE(XunilSerial)`.

#### Usage ####
All you need is just create `xunil::Serial` object with path to serial port as
the first argumet, and port configuration as the second one.

You can open serial port by `open` method call, which will throw an exception
in case of error.

After port is open, just use `Serial` object as usual iostream object to read
and write data.

While serial blocks during read or write operation, it always can be stopped
by the `close` method which will stop reaing and writing loop.

Here is a full example:
```c++
#include <XunilSerial/Serial.hpp>

#include <iostream>

int main(int argc, char** argv) {
  using namespace xunil::serial;

  if (argc < 2) {
    std::cerr << argv[0] << " <port name>\n";
    return -1;
  }

  xunil::Serial serial(argv[1], {.baudrate = 125000});
  serial.open();

  while (true) {
    std::string s;
    std::cout << "read\n";
    std::cin >> s;
    std::cout << "send\n";
    serial << s << "\n";
    serial.flush();
    s.clear();
    std::cout << "read from serial\n";
    serial >> s;
    std::cout << s << "\n";
  }

  return 0;
}
```

#### Putting characters back ####
Putting charackters back to the serial is possible only by the `putback`
function. Ungetting the last character by `unget` function is not possible
and always puts stream into the bad state.

#### Getting serial informations ####
You can get port name by calling the `name` function.
You can get underlying port's file descriptor by `fd` function.
You can check if serial port is open by `isOpen` function.

#### Setting serial control pins ####
You can set RTS and DTR pins by function `setPin` and get its value by
function `getPin`.

```c++
serial.setPin(PinName::RTS, PinState::ON);
serial.getPin(PinName::RTS) == PinState::ON;
```
