#ifndef XUNIL_PTMX_CONNECTED_PAIR_HPP
#define XUNIL_PTMX_CONNECTED_PAIR_HPP

#include <XunilSerial/Serial.hpp>

namespace xunil::serial::tests {

  class PTMX_ConnectedPair {
    public:
      PTMX_ConnectedPair(const Config &cfg);
      ~PTMX_ConnectedPair();

      Serial& master();
      Serial& slave();

    private:
      Serial _master;
      Serial _slave;
  };

} /* namespace xunil::serial::tests */

#endif /* XUNIL_PTMX_CONNECTED_PAIR_HPP */
