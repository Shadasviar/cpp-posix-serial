#include <PTMX_ConnectedPair.hpp>

#include <cstdlib>
#include <exception>

#include <XunilSerial/SerialPortError.hpp>

namespace xunil::serial::tests {

  PTMX_ConnectedPair::PTMX_ConnectedPair(const Config &cfg)
    : _master("/dev/ptmx", cfg) {
      _master.open();

      auto slaveName = ::ptsname(_master.fd());
      if (!slaveName)
        throw SerialPortError("Failed to get slave name", _master);

      auto ret = ::grantpt(_master.fd());
      if (ret < 0)
        throw SerialPortError("Failed to grant access to slave", _master);

      ret = ::unlockpt(_master.fd());
      if (ret < 0)
        throw SerialPortError("Failed to unlock serial pair", _master);

      _slave = Serial(slaveName, cfg);
      _slave.open();
  }

  PTMX_ConnectedPair::~PTMX_ConnectedPair() {
  }

  Serial& PTMX_ConnectedPair::master() {
    return _master;
  }

  Serial& PTMX_ConnectedPair::slave() {
    return _slave;
  }

} /* namespace xunil::serial::tests */
