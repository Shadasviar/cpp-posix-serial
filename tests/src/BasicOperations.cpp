#include <gtest/gtest.h>

#include <future>
#include <string>

#include <XunilSerial/Serial.hpp>
#include <PTMX_ConnectedPair.hpp>

TEST(BasicOperations, OpenClose) {
  xunil::Serial serial("/dev/ptmx");
  serial.open();
  serial.close();
}

TEST(BasicOperations, VirtualConnection) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  EXPECT_TRUE(conn.master().isOpen());
  EXPECT_TRUE(conn.slave().isOpen());
}

TEST(BasicOperations, CloseBlocking) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});

  std::string a = "Hello world!";

  auto read = [&conn] () -> std::string {
    std::string x;
    conn.slave() >> x;
    return x;
  };

  auto readSucc = std::async(std::launch::async, read);
  conn.master() << a << std::flush;
  auto x = readSucc.get();
  EXPECT_EQ(x, "Hello");

  auto readFail = std::async(std::launch::async, read);
  conn.slave().close();
  x = readFail.get();
  EXPECT_EQ(x, "world!");
}
