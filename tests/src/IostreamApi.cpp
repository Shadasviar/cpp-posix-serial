#include <gtest/gtest.h>

#include <cstdint>
#include <iostream>

#include <XunilSerial/Serial.hpp>
#include <PTMX_ConnectedPair.hpp>

TEST(IostreamApi, GetOneByte) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  uint8_t a = 10;
  uint8_t b = 20;
  conn.master() << a << std::flush;
  b = conn.slave().get();

  EXPECT_EQ(a, b);
}

TEST(IostreamApi, GetMultipleBytes) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  std::string a = "abcdefg";
  std::string b;
  conn.master() << a << std::flush;

  for (size_t i = 0; i < a.size(); ++i) {
    b.push_back(conn.slave().get());
  }

  EXPECT_EQ(a, b);
}

TEST(IostreamApi, ReadString) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  std::string a = "abcdefg";
  std::string b;
  conn.master() << a << std::endl;
  conn.slave() >> b;

  EXPECT_EQ(a, b);
}

TEST(IostreamApi, ReadStrings) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  std::string a = "abcdefg";
  std::string b = "12ladfvdfJNGKSF";
  std::string c = "LDvmvdfvdd";
  std::string x, y, z;

  conn.master() << a << std::endl;
  conn.master() << b << std::endl;
  conn.master() << c << std::endl;
  conn.slave() >> x;
  conn.slave() >> y;
  conn.slave() >> z;

  EXPECT_EQ(a, x);
  EXPECT_EQ(b, y);
  EXPECT_EQ(c, z);
}

TEST(IostreamApi, ReadInt) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  int a = 42;
  int b;
  conn.master() << a << std::endl;
  conn.slave() >> b;

  EXPECT_EQ(a, b);
}

TEST(IostreamApi, ReadInts) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  int a = 42;
  int b = -43534323;
  int c = INT_MAX;
  int d = INT_MIN;
  int x,y,z,t;

  conn.master() << a << std::endl;
  conn.master() << b << std::endl;
  conn.master() << c << std::endl;
  conn.master() << d << std::endl;
  conn.slave() >> x >> y >> z >> t;

  EXPECT_EQ(a, x);
  EXPECT_EQ(b, y);
  EXPECT_EQ(c, z);
  EXPECT_EQ(d, t);
}

TEST(IostreamApi, Peek) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  std::string a = "abcdefg";
  std::string b;

  conn.master() << a << std::endl;

  for (int i = 0; i < 1000; ++i) b += conn.slave().peek();
  EXPECT_EQ(b, std::string(1000, 'a'));

  conn.slave() >> b;
  EXPECT_EQ(a, b);
}

TEST(IostreamApi, UngetAndClear) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  std::string a = "abcdefg";
  std::string b;

  EXPECT_TRUE(conn.slave().good());

  conn.master() << a << std::endl;
  conn.slave() >> b;
  conn.slave().unget();

  EXPECT_FALSE(conn.slave().good());

  conn.slave().clear();
  EXPECT_TRUE(conn.slave().good());

  EXPECT_EQ(a, b);
}

TEST(IostreamApi, PutBack) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  uint8_t a = 42;
  uint8_t b = 55;
  uint8_t x = 0;;

  EXPECT_TRUE(conn.slave().good());

  conn.master() << a << b << std::flush;

  x = conn.slave().get();
  EXPECT_EQ(a, x);

  conn.slave().putback(11);
  EXPECT_TRUE(conn.slave().good());

  x = conn.slave().get();
  EXPECT_EQ(11, x);

  x = conn.slave().get();
  EXPECT_EQ(b, x);
}

TEST(IostreamApi, Ignore) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  std::string a = "abcdefg1234hijk#";
  std::string x;

  conn.master() << a << std::endl;

  conn.slave().ignore(5, '#');
  conn.slave() >> x;

  EXPECT_EQ(x, std::string(a, 5));
}

TEST(IostreamApi, Read) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  std::string a = "abcdefg1234hijk#";
  char x[5];

  conn.master() << a << std::flush;

  conn.slave().read(x, 5);
  EXPECT_EQ(std::string(x, 5), std::string(a, 0, 5));

  conn.slave().read(x, 3);
  EXPECT_EQ(std::string(x, 3), std::string(a, 5, 3));

  conn.slave().read(x, 1);
  EXPECT_EQ(std::string(x, 1), std::string(a, 8, 1));

  conn.slave().read(x, 0);
  EXPECT_EQ(std::string(x, 0), std::string(a, 9 , 0));

  conn.slave().read(x, 4);
  EXPECT_EQ(std::string(x, 4), std::string(a, 9 , 4));
}

TEST(IostreamApi, ReadSome) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  std::string a = "abc";
  char x[5];

  conn.master() << a << std::flush;

  /* Wait for the first character available */
  [[maybe_unused]] auto _ = conn.slave().peek();

  auto n = conn.slave().readsome(x, 5);
  EXPECT_EQ(std::string(x, n), a);
}

TEST(IostreamApi, Gcount) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  std::string a = "abcAVD64738hdgfd";
  char x[5];
  char y[100];

  conn.master() << a << std::endl;
  EXPECT_EQ(conn.slave().gcount(), 0);

  conn.slave().read(x, 5);
  EXPECT_EQ(conn.slave().gcount(), 5);
  EXPECT_EQ(std::string(x, 5), std::string(a, 0, 5));

  conn.slave().getline(y, 100);
  /* Count newline character too */
  EXPECT_EQ(conn.slave().gcount(), a.size() - 5 + 1);
  EXPECT_EQ(y, std::string(a, 5));

  conn.slave().putback('a');
  EXPECT_EQ(conn.slave().gcount(), 0);
}

TEST(IostreamApi, Tellg) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  std::string a = "Hello, world!";
  std::string x;

  conn.master() << a << std::flush;

  conn.slave() >> x;
  EXPECT_EQ(x, "Hello,");
  EXPECT_EQ(conn.slave().tellg(), -1);
}

TEST(IostreamApi, Seekg) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  std::string a = "Hello, world!";
  std::string x;

  conn.master() << a << std::endl;

  conn.slave() >> x;
  EXPECT_EQ(x, "Hello,");

  conn.slave().seekg(0);
  EXPECT_FALSE(conn.slave().good());

  x.clear();
  conn.slave() >> x;
  EXPECT_EQ(x, "");
}

TEST(IostreamApi, Put) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});

  conn.master().put(1).put(2).put(3).put(4).flush();

  char x = 0;

  x = conn.slave().get();
  EXPECT_EQ(x, 1);

  x = conn.slave().get();
  EXPECT_EQ(x, 2);

  x = conn.slave().get();
  EXPECT_EQ(x, 3);

  x = conn.slave().get();
  EXPECT_EQ(x, 4);
}

TEST(IostreamApi, Write) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  char a[10] = "123456789";
  char x[10] = {0,};

  conn.master().write(a, 10).flush();
  conn.slave().read(x, 10);

  EXPECT_EQ(std::string(a, 10), std::string(x, 10));
}

TEST(IostreamApi, Tellp) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  std::string a = "Hello World!";

  conn.master() << a << std::flush;
  EXPECT_EQ(conn.master().tellp(), -1);
}

TEST(IostreamApi, Seekp) {
  xunil::serial::tests::PTMX_ConnectedPair conn({});
  std::string a = "Hello World!";
  conn.master().seekp(0, std::ios_base::end);
  EXPECT_TRUE(conn.master().fail());
}
