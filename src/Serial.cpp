#include <XunilSerial/Serial.hpp>
#include <XunilSerial/SerialPortError.hpp>

#include <thread>

#include <fcntl.h>
#include <unistd.h>

namespace xunil {

  Serial::Serial() : std::iostream(this) {
  }

  Serial::Serial(std::string_view name, const serial::Config &cfg)
    : std::iostream(this)
    , _config(cfg)
    , _name(name)
    {
  }

  Serial::Serial(Serial &&rhs) : std::iostream(this) {
    if (&rhs == this) return;
    swap(std::move(rhs));
  }

  Serial& Serial::operator=(Serial &&rhs) {
    if (&rhs == this) return *this;
    swap(std::move(rhs));
    return *this;
  }

  void Serial::swap(Serial &&s) {
    if (&s == this) return;
    close();

    std::streambuf::operator=(std::move(s));
    std::iostream::operator=(std::move(s));
    rdbuf(this);

    std::swap(_fd, s._fd);
    std::swap(_config, s._config);
    std::swap(_name, s._name);
    std::swap(_outBuf, s._outBuf);
    std::swap(_inBuf, s._inBuf);
    _isOpen.store(s._isOpen.load());

    std::swap(_oldConfig.c_iflag, s._oldConfig.c_iflag);
    std::swap(_oldConfig.c_oflag, s._oldConfig.c_oflag);
    std::swap(_oldConfig.c_cflag, s._oldConfig.c_cflag);
    std::swap(_oldConfig.c_lflag, s._oldConfig.c_lflag);
    std::swap(_oldConfig.c_ispeed, s._oldConfig.c_ispeed);
    std::swap(_oldConfig.c_ospeed, s._oldConfig.c_ospeed);
    for (size_t i = 0; i < NCCS; ++i) {
      std::swap(_oldConfig.c_cc[i], s._oldConfig.c_cc[i]);
    }
  }

  Serial::~Serial() {
    close();
  }

  void Serial::open() {
    _fd = ::open(_name.c_str(), O_RDWR | O_NOCTTY, O_NONBLOCK);
    if (_fd < 0)
      throw serial::SerialPortError("Failed to open serial port", *this);

    termios2 tty;

    auto err = ::ioctl(_fd, TCGETS2, &tty);
    if (err < 0)
      throw serial::SerialPortError("Failed to get tty configuration", *this);

    _oldConfig = tty;

    tty.c_cflag &= ~PARENB;
    tty.c_cflag |= static_cast<int>(_config.parity);

    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= static_cast<int>(_config.dataBits);

    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag |= static_cast<int>(_config.stopBits);

    tty.c_cflag &= ~(static_cast<uint32_t>(serial::HwFlowControl::CTSRTS));
    tty.c_cflag |= static_cast<uint32_t>(_config.hwFlowControl);

    tty.c_cflag |= CREAD | CLOCAL;

    /* Configure custom baudrate always */
    tty.c_cflag &= ~CBAUD;
    tty.c_cflag |= CBAUDEX;

    /* Configure non canonical mode for serial */
    tty.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHONL);
    tty.c_iflag &= ~(IXON | IXOFF | IXANY);
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);
    tty.c_oflag &= ~(OPOST | ONLCR);

    /* Block read until at least 1 character is not available */
    std::fill(tty.c_cc, tty.c_cc + NCCS, _POSIX_VDISABLE);
    tty.c_cc[VTIME] = 0;
    tty.c_cc[VMIN] = 1;

    tty.c_ispeed = _config.baudrate;
    tty.c_ospeed = _config.baudrate;

    err = ::ioctl(_fd, TCSETS2, &tty);
    if (err < 0)
      throw serial::SerialPortError("Failed to configure serial port", *this);

    _isOpen = true;
  }

  void Serial::close() {
    if (_isOpen.load()) {
      _isOpen = false;
      ::ioctl(_fd, TCSETS2, &_oldConfig);
      ::close(_fd);
    }
  }

  std::string_view Serial::name() const {
    return _name;
  }

  bool Serial::isOpen() const {
    return _isOpen.load();
  }

  int Serial::fd() const {
    return _fd;
  }

  void Serial::setPin(serial::PinName pin, serial::PinState state) {
    auto ret = ::ioctl(_fd, static_cast<int>(state), &pin);
    if (ret < 0)
      throw serial::SerialPortError("Failed to set serial pin", *this);
  }

  serial::PinState Serial::getPin(serial::PinName pin) {
    int ret = 0;

    auto error = ::ioctl(_fd, TIOCMGET, &ret);
    if (error < 0)
      throw serial::SerialPortError("Failed to read serial pin state", *this);

    return (ret & static_cast<int>(pin))
      ? serial::PinState::ON
      : serial::PinState::OFF
      ;
  }

  int Serial::sync() {
    while (_isOpen.load() && _outBuf.size() > 0) {
      if (waitForEvent(POLLOUT, POLLWRBAND)) {
        auto writtenBytes = ::write(_fd, _outBuf.data(), _outBuf.size());
        if (writtenBytes < 0) return -1;
        _outBuf.erase(_outBuf.begin(), _outBuf.begin() + writtenBytes);
      }
    }

    return 0;
  }

  int Serial::overflow(int c) {
    _outBuf.push_back(static_cast<char>(c));
    return c;
  }

  std::streamsize Serial::xsputn(const char* s, std::streamsize n) {
    _outBuf.insert(_outBuf.end(), s, s + n);
    return n;
  }

  int Serial::underflow() {
    if (!_inBuf.empty()) return _inBuf.back();

    while (_isOpen.load()) {
      if (waitForEvent(POLLIN, POLLPRI)) {
        auto ret = readAllAvailableData();
        if (ret < 0) return EOF;
        if (!_inBuf.empty()) return _inBuf.back();
      }
    }

    return EOF;
  }

  int Serial::uflow() {
    if (!_inBuf.empty()) {
      char c = _inBuf.back();
      _inBuf.pop_back();
      return static_cast<int>(c);
    }

    while (_isOpen.load()) {
      if (waitForEvent(POLLIN, POLLPRI)) {
        auto ret = readAllAvailableData();
        if (ret < 0) return EOF;

        auto c = _inBuf.back();
        _inBuf.pop_back();
        return c;
      }
    }

    return EOF;
  }

  std::streamsize Serial::xsgetn(char* s, std::streamsize n) {
    if (_inBuf.size() >= static_cast<size_t>(n)) {
      std::copy(_inBuf.rbegin(), _inBuf.rbegin() + n, s);
      _inBuf.erase(_inBuf.end() - n, _inBuf.end());
      return n;
    }

    std::streamsize offset = _inBuf.size();

    std::copy(_inBuf.rbegin(), _inBuf.rend(), s);
    _inBuf.clear();

    while (_isOpen.load() && offset < n) {
      if (waitForEvent(POLLIN, POLLPRI)) {
        auto ret = readAllAvailableData();
        if (ret < 0) return EOF;

        if (_inBuf.size() >= static_cast<size_t>(n) - offset) {
          std::copy(_inBuf.rbegin(), _inBuf.rbegin() + n, s + offset);
          _inBuf.erase(_inBuf.end() - n + offset, _inBuf.end());
          return n;
        }

        offset += _inBuf.size();
        std::copy(_inBuf.rbegin(), _inBuf.rend(), s + offset);
        _inBuf.clear();
      }
    }

    return offset;
  }

  int Serial::readAllAvailableData() {
    int bytesReady = 0;

    auto ret = ::ioctl(_fd, FIONREAD, &bytesReady);
    if (ret != 0) bytesReady = _fallbackReadSize;

    std::vector<char> tmp(bytesReady);
    auto c = ::read(_fd, tmp.data(), bytesReady);
    _inBuf.insert(_inBuf.begin(), tmp.rbegin(), tmp.rend());

    return c;
  }

  int Serial::pbackfail(int c) {
    if (c == EOF) return EOF;
    _inBuf.push_back(c);
    return c;
  }

  std::streamsize Serial::showmanyc() {
    readAllAvailableData();
    return _inBuf.size();
  }

} /* namespace xunil */
