#include <XunilSerial/SerialPortError.hpp>

#include <cstring>

namespace xunil::serial {

  SerialPortError::SerialPortError
    ( std::string_view msg
    , Serial &port
    , int code
    ) : std::runtime_error
        ( "Serial port error: "
        + std::string(port.name()) + ": "
        + std::string(msg) + " ("
        + std::string(::strerror(code)) + ")"
        ) {
    port.close();
  }

} /* namespace xunil::serial */
