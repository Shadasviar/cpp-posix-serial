#ifndef CPP_XUNIL_SERIAL_PORT_ERROR_HPP
#define CPP_XUNIL_SERIAL_PORT_ERROR_HPP

#include <XunilSerial/Serial.hpp>

#include <stdexcept>
#include <string_view>

namespace xunil::serial {

  class SerialPortError : public std::runtime_error {
    public:
      explicit SerialPortError
        ( std::string_view msg
        , Serial &port
        , int code = errno
        );
  };

} /* xunil::serial */

#endif /* CPP_XUNIL_SERIAL_PORT_ERROR_HPP */
