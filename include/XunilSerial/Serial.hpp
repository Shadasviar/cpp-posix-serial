#ifndef CPP_XUNIL_SERIAL_SERIAL_H
#define CPP_XUNIL_SERIAL_SERIAL_H

#include <string_view>
#include <string>
#include <iostream>
#include <vector>
#include <atomic>

#include <cstdint>
#include <sys/ioctl.h>
#include <asm/termbits.h>
#include <poll.h>

namespace xunil {

  namespace serial {

    enum class Parity {
      NONE,
      EVEN = PARENB,
      ODD  = PARENB | PARODD,
    };

    enum class StopBits {
      ONE,
      TWO = CSTOPB,
    };

    enum class DataBits {
      CS_5 = CS5,
      CS_6 = CS6,
      CS_7 = CS7,
      CS_8 = CS8,
    };

    enum class HwFlowControl : uint32_t {
      OFF,
#ifdef CCTS_OFLOW
      CTS     = CCTS_OFLOW,
#endif
#ifdef CRTS_IFLOW
      RTS     = CRTS_IFLOW,
#endif
#ifdef CCTS_OFLOW
#ifdef CRTS_IFLOW
      CTSRTS  = CCTS_OFLOW | CRTS_IFLOW,
#endif
#endif
#ifdef CRTSCTS
      CTSRTS  = CRTSCTS,
#else
      CTSRTS  = 0, /* Platform does not support hw flow control */
#endif
    };

    enum class PinState :int {
      ON  = TIOCMBIS,
      OFF = TIOCMBIC,
    };

    enum class PinName : int {
      RTS = TIOCM_RTS,
      DTR = TIOCM_DTR,
    };

    struct Config {
      Parity        parity        = Parity::NONE;
      StopBits      stopBits      = StopBits::ONE;
      DataBits      dataBits      = DataBits::CS_8;
      HwFlowControl hwFlowControl = HwFlowControl::OFF;
      speed_t       baudrate      = 57600;
    };

  } /* namespace serial */

  class Serial : private std::streambuf, public std::iostream {
    public:

      Serial();
      explicit Serial(std::string_view name, const serial::Config &cfg = {});
      Serial(Serial &&rhs);
      Serial& operator=(Serial &&rhs);
      ~Serial();

      void open();
      void close();

      std::string_view name() const;
      bool isOpen() const;
      int fd() const;

      void setPin(serial::PinName pin, serial::PinState state);
      serial::PinState getPin(serial::PinName pin);

    private:
      int sync() override;
      int overflow(int c) override;
      std::streamsize xsputn(const char* s, std::streamsize n) override;
      int underflow() override;
      int uflow() override;
      std::streamsize xsgetn(char* s, std::streamsize n) override;
      int pbackfail(int c) override;
      std::streamsize showmanyc() override;

      int readAllAvailableData();
      void swap(Serial &&s);

      template <typename ... Events>
      bool waitForEvent(Events ... events) {
        ::pollfd fd {
          .fd       = _fd,
          .events   = static_cast<decltype(::pollfd::events)>((... | events)),
          .revents  = 0,
        };

        auto dataReady = ::poll(&fd, 1, Serial::_pollTimeout);
        return dataReady > 0 && (... || (fd.revents & events));
      }

      int               _fd     = 0;
      static const int  _pollTimeout = 500;
      static const int  _fallbackReadSize = 500;
      serial::Config    _config;
      termios2          _oldConfig;
      std::string       _name;
      std::vector<char> _outBuf;
      std::vector<char> _inBuf;
      std::atomic<bool> _isOpen = false;
  };

} /* namespace xunil */

#endif /* CPP_XUNIL_SERIAL_SERIAL_H*/
